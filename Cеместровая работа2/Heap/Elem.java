package Heap;

public class Elem {
    private int value;
    private Elem next;
    Elem(int value){
        this.value=value;
    }
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Elem getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }


}
