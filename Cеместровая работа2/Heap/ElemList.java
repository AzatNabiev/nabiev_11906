package Heap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;

public class ElemList {
    private Elem head;
    private Elem tale;
    private int listLength;
    ElemList(int[]arr)  {
        for (int i=0;i<arr.length;i++){
            add(new Elem(arr[i]));
        }
    }
    public void sumListLength(){
        listLength++;
    }
    public int getListLength(){
        return listLength;
    }
    public void add(Elem listElement){
        if (head==null){
            head=listElement;
            tale=listElement;
        }
        sumListLength();
        tale.setNext(listElement);
        tale=tale.getNext();
    }
    public Elem returnElemByIndex(int index){
        Elem tmp = head;
        for(int i=0; i<index; i++){
            tmp = tmp.getNext();
        }
        return tmp;
    }
    public void showList(){
        Elem elem=head;
        while (elem!=null){
            System.out.print(elem.getValue()+" ");
            elem=elem.getNext();
        }
    }
}
