package Heap;

public class HeapSort {
    private int iterations;

    public int getIt() {
        return iterations;
    }

    public void setIterations() {
        this.iterations++;
    }

    public void sort(ElemList elemList)
    {
        int n = elemList.getListLength();

        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(elemList, n, i);
            setIterations();
        }

        for (int i=n-1; i>0; i--) {
            setIterations();
            swap(elemList,0,i);
            heapify(elemList, i, 0);
        }
    }


    void heapify(ElemList elemList, int n, int i) {
        setIterations();
        int largest = i;
        int l = 2*i+1;
        int r = 2*i+2;

        if (l < n && elemList.returnElemByIndex(l).getValue() > elemList.returnElemByIndex(largest).getValue()) {
            largest = l;
        }


        if (r < n && elemList.returnElemByIndex(r).getValue() > elemList.returnElemByIndex(largest).getValue()) {
            largest = r;
        }

        if (largest != i)
        {
            swap(elemList,i,largest);
            heapify(elemList, n, largest);
        }
    }
    public void swap(ElemList elemList,int i,int largest){
        int swap=elemList.returnElemByIndex(i).getValue();
        elemList.returnElemByIndex(i).setValue(elemList.returnElemByIndex(largest).getValue());
        elemList.returnElemByIndex(largest).setValue(swap);
    }
}