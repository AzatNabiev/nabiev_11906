package HeapArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class MainSort {
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\Acer\\IdeaProjects\\first\\src\\HeapArray\\Input.txt"));
        FileWriter writer = new FileWriter("C:\\Users\\Acer\\IdeaProjects\\first\\src\\HeapArray\\Output.txt");
        int x = 200;
        for(int count=0;count<50;count++){
            String line = in.readLine();
            String [] st = line.split(" ");
            int[] arr =new int [x];
            for (int i = 0;i<x;i++){
                arr[i]=Integer.parseInt(st[i]);
            }
            writer.write("Сортировка на "+ x + " элементах\n");
            HeapSort heapSort=new HeapSort();
            long start = System.nanoTime();
            heapSort.sort(arr);
            long finish = System.nanoTime();
            writer.write(Arrays.toString(arr)+"\n");
            writer.write("Затрачено времени: "+(int)(finish-start)+"\n"+"Кол-во итерации: "+heapSort.getIt()+"\n");
            x+=200;
        }
        writer.close();
        in.close();
    }
}
