package Doctors;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static String getSpec(List<Specialization> res, int docIdSpecialization) {
        Specialization object = res.stream().filter((obj) -> obj.getId() == docIdSpecialization).findFirst().orElse(null);
        if( object != null)
            return object.getSpecialization();
        else{
            return "null";
        }
    }
    public static  String getTime(List<Timetable> res,int doctorId){
        Timetable object=res.stream().filter((obj)->obj.getDoctorId()==doctorId).findFirst().orElse(null);
        if(object!=null)
            return object.getTime();
        else{
            return "null";
        }
    }
    public static String getRoomName(List<Room> res,int roomId) {
        Room object = res.stream().filter((obj) -> obj.getId() == roomId).findFirst().orElse(null);
        if (object != null)
            return object.getName();
        else {
            return "null";
        }
    }
    public static int getRoomId(List<Timetable>res,String time){
        Timetable object=res.stream().filter((obj)->obj.getTime().equals(time)).findFirst().orElse(null);
        if(object!=null)
            return object.getRoomId();
        else{
            return 0;
        }
    }
    public static String getInitials(List<Doctor> res,int doctorId){
        Doctor object=res.stream().filter((obj)->obj.getId()==doctorId).findFirst().orElse(null);
        if(object!=null)
            return object.getName()+object.getSurname()+object.getPatronymic();
        else{
            return "null";
        }
    }
    public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException, NoSuchMethodException {
        FileFramework fileFramework = new FileFramework();
        List<Doctor> doctors = fileFramework.getAll(Doctor.class).stream().map((obj) -> (Doctor) obj).collect(Collectors.toList());
        List<Specialization> specializations = fileFramework.getAll(Specialization.class).stream().map((obj) -> (Specialization) obj).collect(Collectors.toList());
        List<Timetable> timetables = fileFramework.getAll(Timetable.class).stream().map((obj) -> (Timetable) obj).collect(Collectors.toList());
        List<Room> rooms = fileFramework.getAll(Room.class).stream().map((obj) -> (Room) obj).collect(Collectors.toList());
        List<String> res = timetables.stream().map(timetable -> getTime(timetables,timetable.getDoctorId())+" "+getRoomName(rooms,getRoomId(timetables,getTime(timetables,timetable.getDoctorId())))+" "+getInitials(doctors,timetable.getDoctorId())+ " " + getSpec(specializations, timetable.getDoctorId())).collect(Collectors.toList());
        for(String s :res){
            System.out.println(s);
        }
    }
}

