package Doctors;

@StoreInFile(name = "Specialization")
public class Specialization {
    public Specialization(){}
    @MyField(name = "id")
    private int id;
    @MyField(name = "specialization")
    private String specialization;

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getSpecialization() {
        return specialization;
    }
}

