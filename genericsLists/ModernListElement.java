package Tale2;

public class ModernListElement <T> {
    private int value;
    private T next;//link
    ModernListElement(int value){
        this.value=value;
    }
    public int getValue(){
        return value;
    }
    public T getNext(){
        return next;
    }
    public void setNext(T listElement){
        next=listElement;
    }
}
