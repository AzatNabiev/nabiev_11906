package prjct1;

public class BigNumbs {
    private int length=0;
    public int[] c;
    public void setLength(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public int[] function(String str, int[] mas) {
        int k = 0;
        for (int i = str.length() - 1; i >= 1; i--) {
            mas[k] = str.charAt(i) - (int) '0';
            System.out.print(mas[k]);
            k++;
        }
        return mas;
    }

    public String function2(String str, int[] mas) {
        for (int i = mas[0]; i > 0; i--) {
            str += (char) (mas[i] + (int) '0');
        }
        return str;
    }

    public int[] function3(int[] mas, int[] mas1) {
        int[] c = new int[101];
        int z = Math.max(mas[0], mas1[0]);
        int buffer = 0;
        for (int i = 1; i <= z; i++) {
            buffer = mas[i] + mas1[i];
            c[i] += buffer % 10;
            c[i + 1] += buffer / 10;
        }
        if (c[z + 1] == 0) {
            c[0] = z;
        } else {
            c[0] = z - 1;
        }
        return c;
    }

    public static int function4(int[] mas, int[] mas1) {
        if (mas[0] > mas1[0]) {
            return 1;
        } else if (mas1[0] > mas[0]) {
            return -1;
        } else {
            for (int i = 1; i <= mas1[mas1[0]]; i++) {
                if (mas[i] > mas1[i]) {
                    return 1;
                } else if (mas1[i] > mas[i]) {
                    return -1;
                }
            }
            return 0;
        }
    }

    public static void function5(int mas[], int mas1[]) {
        int[] mas2 = new int[101];
        int buffer = 0;

        if (mas[0] > mas1[0] || mas[mas[0]] > mas1[mas1[0]]) {
            for (int i = 1; i <= mas[0]; i++) {
                buffer = mas[i] - mas1[i];
                if (buffer < 0) {
                    mas2[i] = mas[i] + 10 - mas1[i];
                    mas[i + 1] -= 1;
                } else {
                    mas2[i] = buffer;
                }
            }
            if (mas[mas[0]] <= 0) {
                mas2[0] = mas[0] - 1;
            } else {
                mas2[0] = mas[0];
            }
            for (int i = 0; i <= mas2[0]; i++) {
                System.out.print(mas2[i] + " ");
            }
        } else if (mas[0] < mas1[0] || mas[mas[0]] < mas1[mas1[0]]) {
            for (int i = 1; i <= mas1[0]; i++) {
                buffer = mas1[i] - mas[i];
                if (buffer < 0) {
                    mas2[i] = mas1[i] + 10 - mas[i];
                    mas1[i + 1] -= 1;
                } else {
                    mas2[i] = buffer;
                }
            }
            if (mas1[mas1[0]] <= 0) {
                mas2[0] = mas1[0] - 1;
                mas2[mas2[0]] *= -1;
            } else {
                mas2[0] = mas1[0];
                mas2[mas2[0]] *= -1;
            }
            for (int i = 0; i <= mas2[0]; i++) {
                System.out.println(mas2[i] + " ");
            }
        } else {
            int k = function4(mas, mas1);
            if (k >= 0) {
                for (int i = 1; i <= mas[0]; i++) {
                    buffer = mas[i] - mas1[i];
                    if (buffer < 0) {
                        mas2[i] = mas[i] + 10 - mas1[i];
                        mas[i + 1] -= 1;
                    } else {
                        mas2[i] = buffer;
                    }
                }
                if (mas[mas[0]] <= 0) {
                    mas2[0] = mas[0] - 1;
                } else {
                    mas2[0] = mas[0];
                }
                for (int i = 0; i <= mas2[0]; i++) {
                    System.out.print(mas2[i] + " ");
                }
            }
            if (k < 0) {
                for (int i = 1; i <= mas1[0]; i++) {
                    buffer = mas1[i] - mas[i];
                    if (buffer < 0) {
                        mas2[i] = mas1[i] + 10 - mas[i];
                        mas1[i + 1] -= 1;
                    } else {
                        mas2[i] = buffer;
                    }
                }
                if (mas1[mas1[0]] <= 0) {
                    mas2[0] = mas1[0] - 1;
                    mas2[mas2[0]] *= -1;
                } else {
                    mas2[0] = mas1[0];
                    mas2[mas2[0]] *= -1;
                }
                for (int i = 0; i <= mas2[0]; i++) {
                    System.out.println(mas2[i] + " ");
                }
            }
        }
    }
    public void length(int[]mas){
        setLength(mas.length);
    }
    public int[] changeMassive(int[]mas){
        c=new int[mas.length-1];
        for (int i=0;i<c.length;i++){
            c[i]=mas[i+1];
        }
        return c;
    }
    public int[] myltipleBigNumbInt(int[]mas,int numb){
        int masLength=mas.length*numb;
        int[]mas1=new int[masLength];
        int buffer=0;
        for (int i=0;i<mas.length;i++){
            mas1[i]=mas[i]*numb;
        }
        for (int i=0;i<mas1.length;i++){
            if (mas1[i]>9) {
                buffer = mas1[i] / 10;
                mas1[i] %= 10;
                mas1[i + 1] += buffer;
            }
        }
        return mas1;
    }
    public int[] myltipleBigNumbBigNumb(int[]mas,int[]mas1){
        int[]mas3=new int[mas.length+mas1.length];
        int buffer=0;
        int k=0;
        for (int i=0;i<mas.length;i++){
            for (int j=0;j<mas1.length;j++){
                mas3[k]+=mas[i]*mas1[j];
            }
            k++;
        }
        k=0;
        for (int i=0;i<mas3.length;i++){
            if (mas1[i]>9) {
                buffer = mas1[i] / 10;
                mas1[i] %= 10;
                mas1[i + 1] += buffer;
            }
        }
        return mas3;
    }
}
