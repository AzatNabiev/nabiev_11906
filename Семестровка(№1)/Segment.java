public class Segment implements Comparable<Segment> {
    private double x;
    private double y;
    private double x1;
    private double y1;
    private double length;
    private Segment next;

    Segment(double x, double y, double x1, double y1) {
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
        double first=(x-x1);
        double second=(y-y1);
        length=Math.sqrt(first*first+second*second);

    }
    Segment(){}
    public Segment getNext() {
        return next;
    }

    public void setNext(Segment next) {
        this.next = next;
    }

    public double getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public double getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public double getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public double getLength(){
        return length;
    }


    @Override
    public int compareTo(Segment o) {
        int n=-1;
        if (this.getLength()>o.getLength()){
            n=1;
        }
        else if (this.getLength()==o.getLength()){
            n=0;
        }
        return n;
    }
    @Override
    public boolean equals(Object obj){
        boolean bool=false;
        if (this==obj){
            bool=true;
        }
        else if (this instanceof Segment && obj instanceof  Segment){
            if ((this.getX()==((Segment) obj).getX())&&(this.getY()==((Segment) obj).getY())&&
                    (this.getX1()==((Segment) obj).getX1())&&(this.getY1()==((Segment) obj).getY1())){
                bool=true;
            }
        }

        return bool;
    }
}
