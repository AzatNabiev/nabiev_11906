import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;

public class SegmentList<T extends Segment> {
    private T head;
    private T tale;
    private int listLength;

    public int getListLength() {
        return listLength;
    }
    public void sumListLength() {
        this.listLength++;
    }
    public T getHead() {
        return head;
    }
    public void setHead(T head) {
        this.head = head;
    }
    public T getTale() {
        return tale;
    }
    public void setTale(T tale) {
        this.tale = tale;
    }
    SegmentList(String filename)  {
        try {
            String path = "src\\";
            File file = Paths.get(path + filename).toFile();
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line=reader.readLine())!=null){
                String[] mas=line.split(" ");
                add((T) new Segment(Integer.parseInt(mas[0]),Integer.parseInt(mas[1]),Integer.parseInt(mas[2]),
                        Integer.parseInt(mas[3])));
            }
            reader.close();
        }catch (IOException ex){
            System.out.println("Файл не найден");
        }

    }
    SegmentList(){}
    public void show(){
        Segment segment=head;
        int k=1;
        while (segment!=null){
            System.out.printf("Отрезок №"+k+"\n\nТочка Х равна "+segment.getX() + " Tочка Y равна "+segment.getY()
                    +"\nТочка Х1 равна "+segment.getX1() +" Точка Y1 равна "+segment.getY1()+"\nИх длина "+ segment.getLength()+"\n");

            k++;
            segment=segment.getNext();
        }
    }

    public void insert(T f){
        if (head == null) {
            head = f;
            tale = f;
            this.sumListLength();
        }
        if (checkEquality(f)){
            tale.setNext(f);
            tale = (T) tale.getNext();
            this.sumListLength();
        }
    }
    private boolean checkEquality(T f){
        Segment segment=head;
        boolean bool=true;
        while (bool && segment!=null){
            if (segment.equals(f)){
                bool =false;
            }
            segment=segment.getNext();
        }
        return bool;
    }

    public SegmentList angleList(){
        SegmentList segmentList=new SegmentList();
        Segment mark=this.head;
        while (mark!=null){
            if (angleCheck(mark)){
                segmentList.add(addElement((T) mark));
            }
            mark=mark.getNext();
        }
        return  segmentList;
    }
    private boolean angleCheck(Segment obj){
        boolean bool=false;
        double x=obj.getX();
        double x1=obj.getX1();
        double y=obj.getY();
        double y1=obj.getY1();
        if (y==y1){
            bool=true;
        }
        else if (x==y && x1==y1){
            bool=true;
        }
        return bool;
    }
    public void add(T obj) {
        if (head == null) {
            head = obj;
            tale = obj;

        }
        this.sumListLength();
        tale.setNext(obj);
        tale = (T) tale.getNext();
    }
    private Segment addElement(T obj){
        return  new Segment(obj.getX(),obj.getY(),obj.getX1(),obj.getY1());
    }
    public SegmentList lengthList(int a, int b){
        SegmentList segmentList=new SegmentList();
        Segment segment=head;
        while (segment!=null){
            if (lengthCheck(segment,a,b)){
                segmentList.add(addElement((T) segment));

            }
            segment=segment.getNext();
        }
        return  segmentList;
    }
    private boolean lengthCheck(Segment obj, int a, int b){
        boolean bool=false;
        if ((obj.getX1()>=a &&obj.getX1()<=b)&& (obj.getX()>=a && obj.getX()<=b)){
            bool=true;
        }
        return bool;
    }
    public Segment mergeSort(Segment segment) {
        if(segment == null || segment.getNext() == null) {
            return segment;
        }

        Segment middle = middleNode(segment);
        Segment secondHalf = middle.getNext();
        middle.setNext(null);

        return merge(mergeSort(segment), mergeSort(secondHalf));
    }

    public Segment middleNode(Segment segment) {
        if(segment == null) {
            return null;
        }

        Segment a = segment;
        Segment b = segment.getNext();

        while(b != null && b.getNext() != null) {
            a = a.getNext();
            b = b.getNext().getNext();
        }

        return a;
    }
    public Segment merge(Segment a, Segment b) {
        Segment temp = new Segment();
        Segment finalList = temp;

        while(a != null && b != null) {
            if(a.getLength() < b.getLength()) {
                temp.setNext(a);
                a = a.getNext();
            } else {
                temp.setNext(b);
                b = b.getNext();
            }
            temp = temp.getNext();
        }
        temp.setNext((a == null) ? b : a);
        return finalList.getNext();
    }
    public String getInfo(){
        Segment segment=head;
        String str="";
        while (segment!=null){
            str+=segment.getLength()+" ";
            segment=segment.getNext();
        }
        return str;
    }
}

