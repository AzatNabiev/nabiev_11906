
import org.junit.Assert;
import org.junit.Test;

public class TestSegmentList {
    @Test
    public void testInsert(){
            SegmentList segmentList=new SegmentList();
            Segment segment=new Segment(0,4,0,1);
            Segment segment1=new Segment(0,4,0,1);
            Segment segment2=new Segment(0,0,0,0);
            int length;
            segmentList.insert(segment);
            segmentList.insert(segment1);
            segmentList.insert(segment2);
            length=segmentList.getListLength();
            Assert.assertEquals(2,length);
    }
    @Test
    public void testAngleCheck(){
        SegmentList segmentList=new SegmentList();
        Segment segment=new Segment(0,0,1,1);
        Segment segment1=new Segment(1,6,5,6);
        Segment segment2=new Segment(0,0,3,1);
        segmentList.add(segment);
        segmentList.add(segment1);
        segmentList.add(segment2);
        SegmentList segmentList1=segmentList.angleList();
        int count=segmentList1.getListLength();
        Assert.assertEquals(2,count);
    }
    @Test
    public void testLengthList(){
        SegmentList segmentList=new SegmentList();
        Segment segment=new Segment(-10,0,3,0);
        Segment segment1=new Segment(2,0,3,0);
        Segment segment2=new Segment(2,0,10,0);
        Segment segment3=new Segment(6,0,10,0);
        Segment segment4=new Segment(3,0,-10,0);
        segmentList.add(segment);
        segmentList.add(segment1);
        segmentList.add(segment2);
        segmentList.add(segment3);
        segmentList.add(segment4);
        SegmentList segmentList1=segmentList.lengthList(0,5);
        int count=segmentList1.getListLength();
        Assert.assertEquals(1,count);
    }
    @Test
    public void testSort(){
        SegmentList segmentList=new SegmentList();
        Segment segment=new Segment(0,0,5,5);
        Segment segment1=new Segment(0,0,4,4);
        Segment segment2=new Segment(0,0,3,3);
        Segment segment3=new Segment(0,0,2,2);
        Segment segment4=new Segment(0,0,1,1);
        segmentList.setHead(segmentList.mergeSort(segmentList.getHead()));
        String str="1.4142135623730951 2.8284271247461903 4.242640687119285 5.656854249492381 7.0710678118654755 ";
        String str1=segmentList.getInfo();
        int count=str.compareTo(str1);
        Assert.assertEquals(93,count);

    }
}
