package Spisok;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        StudentList<Boys> boys=new StudentList<>();
        StudentList<Girls> girls=new StudentList<>();
        int heightBoys=scanner.nextInt();
        while (heightBoys!=-1){
            Boys boys1=new Boys();
            boys1.setHeight(heightBoys);
            StudentListElement<Boys> studentListElement=new StudentListElement<>(boys1);
            boys.add(studentListElement);
            heightBoys=scanner.nextInt();
        }
        int heightGirls=scanner.nextInt();
        while (heightGirls!=-1){
            Girls girls1=new Girls();
            girls1.setHeight(heightBoys);
            StudentListElement<Girls> studentListElement=new StudentListElement<>(girls1);
            girls.add(studentListElement);
            heightGirls=scanner.nextInt();
        }
        StudentList<Student> students=new StudentList<>();
        StudentListElement<Boys> elB=boys.getHead();
        StudentListElement<Girls> elG=girls.getHead();
        while (elB.getNext()!=null && elG.getNext()!=null){
            if (elB.getValue().getHeight()>elG.getValue().getHeight()){
                students.add(elB);
                elB=elB.getNext();
            }
            else {
                students.add(elG);
                elG=elG.getNext();
            }
        }
        while (elB.getNext()!=null){
            students.add(elB);
            elB=elB.getNext();
        }
        while (elG.getNext()!=null){
            students.add(elG);
            elG=elG.getNext();
        }


        boolean starter=true;
        StudentList studentList=new StudentList();
        while (starter){
            int n=scanner.nextInt();
            if (n==1){
                studentList.doExercise(scanner.nextInt(),scanner.nextInt());
            }
            else if (n==2){
                System.out.println(studentList.checkFillonies());
            }
            else if (n==3){
                System.out.println("продолжаем работу?\n Введите True если да, False если нет ");
                String str=scanner.nextLine();
                if (str.equals("True")){
                    starter=true;
                }
                else if (str.equals("False")){
                    starter=false;
                }
                else {
                    System.out.println("Веведена неверная команда ");
                    starter=false;
                }

            }
        }

    }
}
