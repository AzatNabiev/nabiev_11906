package Spisok;

public abstract class Student {
    private int exercises=0;
    private int height=0;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getExercises() {
        return exercises;
    }

    public void addExrecises(int exrecises) {
        this.exercises += exrecises;
    }
    public abstract void doExercises();

}
