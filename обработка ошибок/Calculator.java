package prjct1;

public class Calculator {
    private long firstNumb;
    private long secondNumb;
    private char znak;

    Calculator(long a, long b, char sign){
        firstNumb=a;
        secondNumb=b;
        znak=sign;

    }
    public void calculate() throws OutOfLongLength {

        long max=Long.MAX_VALUE;
        if (znak=='+'){

            if (max-firstNumb<secondNumb){
                throw new OutOfLongLength();
            }
            else {
                System.out.println(firstNumb+secondNumb);
            }
        }
        if (znak=='-'){
            System.out.println(firstNumb-secondNumb);
        }
        if (znak=='*'){
            long max2=Long.MAX_VALUE;
            if (max2/firstNumb<secondNumb ){
                throw new OutOfLongLength();
            }
            else {
                System.out.println(secondNumb*firstNumb);
            }

        }
        if (znak=='/'){
            if (secondNumb==0){
                throw new ArithmeticException();
            }
            else {
                System.out.println(firstNumb/secondNumb);
            }
        }

    }
}

