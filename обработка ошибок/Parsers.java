package prjct1;

public class Parsers {
    private long k = 0;
    private long a;
    private long b;
    private char type = 'a';

    public static boolean check(String str){
        boolean bool=true;
        for (int i=0;i<str.length();i++){
            if (str.charAt(i)<'0' || str.charAt(i)>'9'){
                bool=false;
            }
        }
        return bool;
    }

    public void setA(long a) {
        this.a = a;
    }

    public void setB(long b) {
        this.b = b;
    }

    public long getA() {
        return a;
    }

    public long getB() {
        return b;
    }
    public char getZnak(){
        return type;
    }

    public void divide(String str) throws SignException, NumberException {
        String str1="";
        String str2="";
        StringBuilder stringBuffer = new StringBuilder(str);
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '*') {
                type = '*';
                int position=1;
                str1=stringBuffer.substring(0, i);
                str2=stringBuffer.substring(i + 1, str.length());
                if (check(str1)){
                    try {
                        setA(Long.parseLong(str1));
                    }
                    catch (java.lang.NumberFormatException ex){
                        throw new NumberException(position);
                    }
                    finally {
                        position++;
                    }
                }
                else {
                    throw new NumberException(position);
                }
                if (check(str2)){
                    try {
                        setB(Long.parseLong(str2));
                    }
                    catch (java.lang.NumberFormatException ex){
                        throw new NumberException(position);
                    }
                }
                else {
                    throw new NumberException(position);
                }
                k++;
                break;


            } else if (str.charAt(i) == '/') {
                type = '/';
                int position=1;
                str1=stringBuffer.substring(0, i);
                str2=stringBuffer.substring(i + 1, str.length());
                if (check(str1)){
                    try {
                        setA(Long.parseLong(str1));
                    }
                    catch (java.lang.NumberFormatException ex){
                        throw new NumberException(position);
                    }
                    finally {
                        position++;
                    }
                }
                else {
                    throw new NumberException(position);
                }
                if (check(str2)){
                    try {
                        setB(Long.parseLong(str2));
                    }
                    catch (java.lang.NumberFormatException ex){
                        throw new NumberException(position);
                    }
                }
                else {
                    throw new NumberException(position);
                }
                k++;
                break;
            } else if (str.charAt(i) == '+') {
                type = '+';
                int position=1;
                str1=stringBuffer.substring(0, i);
                str2=stringBuffer.substring(i + 1, str.length());
                if (check(str1)){
                    try {
                        setA(Long.parseLong(str1));
                    }
                    catch (java.lang.NumberFormatException ex){
                        throw new NumberException(position);
                    }
                    finally {
                        position++;
                    }
                }
                else {
                    throw new NumberException(position);
                }
                if (check(str2)){
                    try {
                        setB(Long.parseLong(str2));
                    }
                    catch (java.lang.NumberFormatException ex){
                        throw new NumberException(position);
                    }
                }
                else {
                    throw new NumberException(position);
                }
                k++;
                break;
            } else if (str.charAt(i) == '-') {
                    type = '-';
                int position=1;
                str1=stringBuffer.substring(0, i);
                str2=stringBuffer.substring(i + 1, str.length());
                if (check(str1)){
                    try {
                        setA(Long.parseLong(str1));
                    }
                    catch (java.lang.NumberFormatException ex){
                        throw new NumberException(position);
                    }
                    finally {
                        position++;
                    }
                }
                else {
                    throw new NumberException(position);
                }
                if (check(str2)){
                    try {
                        setB(Long.parseLong(str2));
                    }
                    catch (java.lang.NumberFormatException ex){
                        throw new NumberException(position);
                    }
                }
                else {
                    throw new NumberException(position);
                }
                k++;
                break;
                }
            }
        if (k==0){
            throw new SignException();
        }

        }

    }


