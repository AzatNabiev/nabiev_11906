package com.company.home_work1;

import java.util.Scanner;

public class Third_task {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int value = 0, breaker = 9;
        while (breaker != 1) {
            value = scanner.nextInt();
            if (value == -1) {
                breaker = 1;
                continue;
            } else {
                value += k;
                if (value >= 0 & value <= 25) {
                    System.out.println(value);
                } else if (value > 25) {
                    int a = value - 26;
                    System.out.println(a);
                } else if (value < 0) {
                    int a = 26 + value;
                    System.out.println(a);
                }
            }
        }
    }
}
