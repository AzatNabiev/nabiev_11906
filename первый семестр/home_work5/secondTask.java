package com.company.home_work5;

import java.util.Scanner;

public class secondTask {
    public static void reader(int k, int[] m1, Scanner sc) {
        int i = 0;
        while (i < k) {
            m1[i] = sc.nextInt();
            i++;
        }
    }

    public static int checker(int[] m1, int[] m2) {
        int okay=0;
        for (int i = 0; i < m2.length; i++) {
            for (int j = 0; j < m2.length; j++) {
                if (m2[j] == m1[i]) {
                    okay=1;
                }

            }
        }
        return okay;
    }
    public static void printer(int[] mas3,int k){
        for(int o=0;o<k;o++){
            System.out.print(mas3[o]+" ");
        }
    }


        public static void main (String[]args) {
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            int[] mas1 = new int[n];
            int m = scanner.nextInt();
            int[] mas2 = new int[m];
            int[] mas3 = new int[n + m];
            reader(n, mas1, scanner);
            reader(m, mas2, scanner);

            for (int k = 0; k < n + m; k++) {
                mas3[k] = checker(mas1, mas2);
            }

            printer(mas3,n+m);
    }
}

