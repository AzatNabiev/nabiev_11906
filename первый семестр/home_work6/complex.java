package com.company.home_work6;

import java.util.Scanner;

public class complex {
    private static void output(String str,double m){
        System.out.println(str+" ");
        System.out.print(m+" ");
    }
    private static void sumComplex(double[]m,double[]m2){
        output("сумма равна",m[0]+m2[0]);
        output("",m[1]+m2[0]);
    }
    private static void umnComplex(double[]m,double[]m2){
        double []mas=new double[2];
        mas[0]=(m[0]*m[2]-m[1]*m2[1]);
        mas[1]=(m[1]*m[0]+m[0]*m2[1]);
        output("умножение равно ",mas[0]);
        output("",mas[1]);
    }
    private static void delComplex(double[]m,double[]m2){
        double[]mas=new double[2];
        mas[0]=(m[0]*m2[0]+m[1]*m2[1])/(m2[0]*m2[0]+m2[1]*m2[1]);
        mas[1]=(m[1]*m2[0]-m[0]*m2[1])/(m2[0]*m2[0]+m2[1]*m2[1]);
        output("деление равно ",mas[0]);
        output("",mas[1]);
    }
    private static void corner(double[]m){
        double gradus=Math.floor(Math.atan(m[0]/m[1]));
        output("градус равен ",gradus);
    }
    private static void absZ(double[]m){
        output("модуль Z равен ",Math.sqrt(m[0]*m[0]+m[1]*m[1]));
    }
    private static void increasingFi(double[]m, double grad){
        double r=Math.sqrt(m[0]*m[0]+m[1]*m[1]);
        double gradus=Math.floor(Math.atan(m[1]/m[0]));
        double x=r*Math.cos(gradus);
        double y=r*Math.sin(gradus);
        output("до добавления фи x равен ", x);
        output("до добавления фи у равен ", y);
    }
    private static void excersize(double a,double b,double c,double d,double e){
        double f=5*inFive(a,b)-a*inThree(c,d)*inTwo(d,e)+e;

    }
    private static double inFive(double a,double b){
        double sum=(Math.pow(a,5)+5*Math.pow(a,4)*b+10*a*a*a*b*b+10*a*a*b*b*b+5*a*Math.pow(b,4)+Math.pow(b,5));
        return sum;
    }
    private static double inThree(double c,double d){
        double sum=(c*c*c+3*c*c*d+3*c*d*d+d*d*d);
        return sum;
    }

    private static double inTwo(double d,double e){
        double sum=(d*d-2*d*e+e*e);
        return sum;
    }

    private static void input(Scanner scan, double[]mas){
        mas[0]=scan.nextDouble();
        mas[1]=scan.nextDouble();
    }

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        double[]mas1=new double[2];
        double[]mas2=new double[2];
        input(scanner,mas1);
        input(scanner,mas2);

    }
}
