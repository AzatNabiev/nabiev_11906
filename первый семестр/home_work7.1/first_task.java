package com.company.home_work7;

import java.util.Scanner;

public class first_task {
    private static int converter(String str){
        int num=0;
        for (int i=0;i<str.length();i++){
            num=num*10+(int)str.charAt(i)-(int)'0';
        }
        return num;
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String c =scanner.nextLine();
        c=c+"+";
        int s=0,k=0;
        String value="";
        for (int i=0;i<c.length();i++){
            if((c.charAt(i)>='0' && c.charAt(i)<='9')) {
                continue;
            }
            else if (c.charAt(i)=='+'){
                for (int j=k;j<i;j++){
                    value+=c.charAt(j);
                }
                k=i+1;
                s+=converter(value);
                value="";
            }
            else if (c.charAt(i)=='-'){
                for (int j=k;j<i;j++){
                    value+=c.charAt(j);
                }
                k=i+1;
                s-=converter(value);
                value="";
            }
        }
        System.out.println(s);

    }
}
