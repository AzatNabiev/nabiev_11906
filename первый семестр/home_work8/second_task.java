package com.company.home_work8;

import java.util.Scanner;

public class second_task {
    private static int size(String str, char[] mas, int count){
        for (int i=0;i<str.length();i++){
            for (int j=0;j<mas.length;j++){
                if (str.charAt(i)==mas[j]){
                    count++;
                }
            }
        }
        return count;
    }
    private static void output(String [] mas){
        for (int i=0;i<mas.length;i++){
            System.out.print(mas[i]+" ");
        }
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();

        char[] signs=new char[n];
        for (int i=0;i<signs.length;i++){
            signs[i]=scanner.next().charAt(0);
        }
        String c=scanner.nextLine()+signs[0];
        int k=0,indexof=0,begin=0;
        k=size(c,signs,k);
        String [] store=new String[k];
        for (int i=0;i<c.length();i++){
            for (int j=0;j<signs.length;j++){
                if (c.charAt(i)==signs[j]){
                    store[indexof]="";
                    for (int q=begin;q<i;q++){
                        store[indexof]+=c.charAt(q);
                    }
                    indexof++;
                    begin=i+1;
                }
            }
        }
        output(store);
    }
}
