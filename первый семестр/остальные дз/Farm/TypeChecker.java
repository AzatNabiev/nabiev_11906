package com.company.Farm;

public class TypeChecker {
    public static int woodCheck(Animal[] mas){
        int k=0;
        for (int i=0;i<mas.length;i++){
            if (mas[i] instanceof Sheep){
                k++;
            }
        }
        return k;
    }
    public static int rideCheck(Animal[] mas){
        int k=0;
        for (int i=0;i<mas.length;i++){
            if (mas[i] instanceof Horse){
                k++;
            }
        }
        return k;
    }
    public static int milkCheck(Animal[] mas) {
        int k = 0;
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] instanceof Sheep || mas[i] instanceof Horse) {
                k++;
            }
        }
        return k;
        }
     public static int meatCheck(Animal[]mas) {
         int k = 0;
         for (int i = 0; i < mas.length; i++) {
             if (mas[i] instanceof Sheep || mas[i] instanceof Horse || mas[i] instanceof Cow) {
                 k++;
             }
         }
         return k;
     }
}


