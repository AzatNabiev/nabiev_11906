package com.company.spaceships;

import java.util.Scanner;

public class Main {
    public static void input(Ship[] mas, Scanner scan) {
        int type = 0;
        for (int i = 0; i < mas.length; i++) {
            System.out.println("type 1 is for square ships\ntype 2 is for triangle ships \ntype 3 for circle ship");
            type = scan.nextInt();
            if (type == 1) {
                int x, y, a;
                System.out.println("вводите х,у,а");
                x = scan.nextInt();
                y = scan.nextInt();
                a = scan.nextInt();
                mas[i] = new SquareShip(x, y, a);
            }
            if (type == 2) {
                int x, x1, x2, y, y1, y2;
                System.out.println("оч много писать но это треугольник))");
                x = scan.nextInt();
                x1 = scan.nextInt();
                x2 = scan.nextInt();
                y = scan.nextInt();
                y1 = scan.nextInt();
                y2 = scan.nextInt();
                mas[i] = new TriangleShip(x, x1, x2, y, y1, y2);
            }
            if (type == 3) {
                System.out.println("кружочек в пупочек");
                int x, y, r;
                x = scan.nextInt();
                y = scan.nextInt();
                r = scan.nextInt();
                mas[i] = new CicrcleShip(x, y, r);

            }
        }
    }
    public static void activities(Ship[] mas, Scanner scan) {
        boolean beginner = true;

        while (beginner) {
            System.out.println("1 is for move, a 2 is for shoot");
            System.out.println("choose the moovement");
            int moveType = scan.nextInt();
            System.out.println("choose the ship ");
            int shipType = scan.nextInt();
            if (moveType == 1) {
                int x, y;
                x = scan.nextInt();
                y = scan.nextInt();
                mas[shipType].move(x, y);
            }
            if (moveType == 2) {
                int aimShip = scan.nextInt();
                mas[shipType].shoot(mas[shipType],mas[aimShip]);
            }
            if (moveType == 0) {
                beginner = false;
            }
        }
    }
    public static void informationAboutShips(Ship[] mas,Scanner scan){
        boolean beginner=true;
        while (beginner){
            int typeOfShip=scan.nextInt();
            int typeOfInformation=scan.nextInt();
            if (typeOfInformation==1){
                System.out.println("кол-во хп"+mas[typeOfShip].getHp());
                System.out.println("имя"+mas[typeOfShip].name);
                System.out.println(mas[typeOfShip].getX()+" "+mas[typeOfShip].getY());
            }
        }

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("введите кол-во кораблей");
        int n = scanner.nextInt();
        Ship[] ships = new Ship[n];
        System.out.println("сщас буим делать корабли ");
        input(ships, scanner);
        activities(ships, scanner);
        informationAboutShips(ships,scanner);

    }
}
