package com.company.spaceships;
public abstract class Ship {
    private double hp = 100.0;
    protected double centreOfMasX = 0;
    protected double centreOfMasY = 0;
    protected String name;
    private double x, y;

    public void setHp(double a) {
        hp = a;
    }

    public double getHp() {
        return hp;
    }
    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }
    public void  move(int x,int y){
        this.x+=x;
        this.y+=y;
    };
    public void shoot(Ship shooter, Ship aim) {
        double r = 100 / (Math.sqrt(Math.pow(aim.centreOfMasX - shooter.centreOfMasX, 2) + Math.pow(aim.centreOfMasY - shooter.centreOfMasY, 2)));
        aim.setHp(aim.getHp()-r);
    }
}

