package com.company.spaceships;

public class SquareShip extends Ship {
    SquareShip(int x, int y, int a) {
        centreOfMasX = x + a / 2;
        centreOfMasY = y + a / 2;
        name="квадратный корабль";
    }

}
