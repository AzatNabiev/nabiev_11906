package com.company.spaceships;

public class TriangleShip extends Ship {
    TriangleShip(int x, int x1, int x2, int y, int y1, int y2) {
        centreOfMasX = (x + x1 + x2) / 3;
        centreOfMasY = (y + y1 + y2) / 3;
        name="треугольный";
    }
}
