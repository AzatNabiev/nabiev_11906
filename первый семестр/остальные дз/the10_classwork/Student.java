package com.company.the10_classwork;

public class Student {
    private String name,surname,patronic;
    private int[]mas=new int[3];
    private int al,inf,mat;
    Student(String name, String surname, String patronic){
        this.name=name;
        this.surname=surname;
        this.patronic=patronic;
    }
    public String getNameFull(){
        return surname+" "+name+" "+patronic;
    }
    public String getNameShort(){
        return surname+" "+name.charAt(0)+" "+patronic.charAt(0);
    }
    public void setMarkAlgem(int mark){
        mas[0]=mark;
    }
    public int getMarkAlgem(){
        return mas[0];
    }
    public void setMarkInf(int mark){
        mas[1]=mark;
    }
    public int getMarkInf(){
        return mas[1];
    }
    public void setMarkMatan(int mark){
        mas[2]=mark;
    }
    public int getMarkMatan(){
        return mas[2];
    }
    public int getMiddleMark(){
        return (mas[0]+mas[1]+mas[2])/3;
    }
    public String getMarks(){return mas[0]+" "+mas[1]+" "+mas[2];}
}
