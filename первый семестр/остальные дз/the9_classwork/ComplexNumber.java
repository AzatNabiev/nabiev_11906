package com.company.the9_classwork;

public class ComplexNumber {
    int a1,b1;
    ComplexNumber(int a1, int b1){
        this.a1=a1;
        this.b1=b1;
    }
    public ComplexNumber sumComplex(int a,int b,ComplexNumber obj){
        obj.a1=a+b;
        obj.b1=a-b;
        return obj;
    }
}
